package lang.taxi.query

import lang.taxi.expressions.Expression
import lang.taxi.expressions.Literal
import lang.taxi.expressions.TypeExpression
import lang.taxi.services.operations.constraints.Constraint
import lang.taxi.types.QualifiedName
import lang.taxi.types.Type
import lang.taxi.types.TypedValue

sealed class FactValue() {
   abstract val type:Type
   @Deprecated("No longer used - everything is an expression")
   data class Constant(val value: TypedValue) : FactValue() {
      override val type: Type = value.type
   }
   data class Expression(val assignmentType: Type, val expression: lang.taxi.expressions.Expression) : FactValue() {
      override val type: Type = assignmentType
   }
   data class Variable(override val type: Type, val name: String) : FactValue()

   /**
    * Convenience method
    */
   val typedValue: TypedValue
      get() {
         return when (this) {
            is Constant -> this.value
            is FactValue.Expression -> {
               if (this.expression is Literal) {
                  this.expression.asTypedValue()
               } else {
                  error("This variable does not contain a constant")
               }
            }
            else -> error("This variable does not contain a constant")
         }
      }

   val hasValue: Boolean
      get() {
         return when {
            this is Constant -> true
            this is FactValue.Expression && this.expression is Literal -> true
            else -> false
         }
      }
   val variableName: String
      get() {
         return when (this) {
            is Variable -> this.name
            else -> error("This variable is constant and does not have a variable name")
         }
      }
}

data class DiscoveryType(
   val expression: Expression,
   /**
    * Starting facts aren't the same as a constraint, in that they don't
    * constraint the output type.  However, they do inform query strategies,
    * so we pop them here for query operations to consider.
    */
   val startingFacts: List<Parameter>,
) {
   val typeName: QualifiedName = expression.returnType.toQualifiedName()
   val type = expression.returnType

   // largely here for convenience & backwards compatibility
   val constraints: List<Constraint>
      get() {
         if (expression !is TypeExpression) return emptyList()
         return expression.constraints
      }
}


enum class QueryMode(val directive: String) {
   @Deprecated("FIND_ONE is no longer supported")
   FIND_ONE("findOne"),
   FIND_ALL("find"),
   // See the grammar for thoughts around this.
   MAP("map"),
   STREAM("stream"),
   MUTATE("call");

   companion object {
      fun forToken(token: String): QueryMode {
         // Legacy support - findOne and findAll are deprcated in favour of find
         return if (token == "find") {
            FIND_ALL
         } else {
            values().first { it.directive == token }
         }
      }
   }
}

typealias TaxiQLQueryString = String
