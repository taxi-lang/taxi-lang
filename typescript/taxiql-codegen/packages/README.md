This contains the package.json for the platform-specific packages for the native code-gen tool.

Builds of this tool happen in our CI/CD pipelines, because they're platform-specific.

