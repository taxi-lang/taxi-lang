A simple example that makes use of the `useQuery` React hook.

Unlike the other examples, this query runs as soon as the component mounts, without waiting for user interaction.
