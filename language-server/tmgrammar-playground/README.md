## Playground for editing the TM Grammar for Taxi

### Setup

Create symlinks to the source files:
```bash
cd ../public
ln -s ../../vscode-extension/src/test/grammar/snap samples
ln ../../vscode-extension/syntaxes/taxi.tmLanguage.json taxi.tmLanguage.json
```
