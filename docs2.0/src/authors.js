import martyPittAvatar from './img/authors/martypitt.jpg'
import michaelStoneAvatar from '@/img/authors/michaelstone.png'

export const martyPitt = {
  name: 'Marty Pitt',
  twitter: 'marty_pitt',
  avatar: martyPittAvatar,
}

export const michaelStone = {
  name: 'Michael Stone',
  twitter: 'michaelstoneldn',
  avatar: michaelStoneAvatar
}

export const jasonLangdon = {
  name: 'Jason Langdon',
  //twitter: 'michaelstoneldn',
  //avatar: michaelStoneAvatar
}
