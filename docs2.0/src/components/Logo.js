export function Logo({className, ...props}) {
  return (

    <svg width="48" height="48" viewBox="0 0 95 95" fill="none" xmlns="http://www.w3.org/2000/svg" className={className} {...props}>
      <g clipPath="url(#clip0_6_24)">
        <rect width="95" height="95" fill="#F2CC05"/>
        <path
          d="M6.92229 26.3729V21.0909H29.6663V26.3729H21.5068V48H15.0949V26.3729H6.92229ZM33.083 48H26.0929L35.1721 21.0909H43.8308L52.91 48H45.92L39.6 27.8707H39.3898L33.083 48ZM32.1501 37.4098H46.7609V42.3501H32.1501V37.4098ZM60.17 21.0909L65.084 29.592H65.2943L70.2609 21.0909H77.54L69.42 34.5455L77.8028 48H70.3397L65.2943 39.407H65.084L60.0386 48H52.6281L60.9715 34.5455L52.8383 21.0909H60.17ZM85.1875 21.0909V48H78.6836V21.0909H85.1875Z"
          fill="black"/>
        <rect x="14" y="52" width="16" height="16" rx="2" fill="black"/>
        <rect x="42" y="52" width="16" height="16" rx="2" fill="black"/>
        <rect x="70.0001" y="52" width="16" height="16" rx="2" fill="black"/>
        <rect x="28" y="67" width="16" height="16" rx="2" fill="black"/>
        <rect x="56" y="67" width="16" height="16" rx="2" fill="black"/>
        <rect x="100" y="83" width="16" height="16" rx="2" transform="rotate(-180 100 83)" fill="black"/>
        <path
          d="M26.2432 67.6458L29.5849 64.9724C29.7081 65.6226 29.9693 66.1764 30.3964 66.6036C30.763 66.9701 31.2228 67.2145 31.7568 67.3542L28.4151 70.0276C28.2919 69.3774 28.0307 68.8236 27.6036 68.3964C27.237 68.0299 26.7772 67.7855 26.2432 67.6458Z"
          fill="black" stroke="black" strokeLinecap="square" strokeLinejoin="round"/>
        <path
          d="M43.5849 70.0276L40.2432 67.3542C40.7772 67.2145 41.237 66.9701 41.6036 66.6036C42.0307 66.1764 42.2919 65.6226 42.4151 64.9724L45.7568 67.6458C45.2228 67.7855 44.763 68.0299 44.3964 68.3964C43.9693 68.8236 43.7081 69.3774 43.5849 70.0276Z"
          fill="black" stroke="black" strokeLinecap="square" strokeLinejoin="round"/>
        <path
          d="M54.2432 67.6458L57.5849 64.9724C57.7081 65.6226 57.9693 66.1764 58.3964 66.6036C58.763 66.9701 59.2228 67.2145 59.7568 67.3542L56.4151 70.0276C56.2919 69.3774 56.0307 68.8236 55.6036 68.3964C55.237 68.0299 54.7772 67.7855 54.2432 67.6458Z"
          fill="black" stroke="black" strokeLinecap="square" strokeLinejoin="round"/>
        <path
          d="M71.5849 70.0276L68.2432 67.3542C68.7772 67.2145 69.237 66.9701 69.6036 66.6036C70.0307 66.1764 70.2919 65.6226 70.4151 64.9724L73.7568 67.6458C73.2228 67.7855 72.763 68.0299 72.3964 68.3964C71.9693 68.8236 71.7081 69.3774 71.5849 70.0276Z"
          fill="black" stroke="black" strokeLinecap="square" strokeLinejoin="round"/>
        <path
          d="M82.2432 67.6458L85.5849 64.9724C85.7081 65.6226 85.9693 66.1764 86.3964 66.6036C86.763 66.9701 87.2228 67.2145 87.7568 67.3542L84.4151 70.0276C84.2919 69.3774 84.0307 68.8236 83.6036 68.3964C83.237 68.0299 82.7772 67.7855 82.2432 67.6458Z"
          fill="black" stroke="black" strokeLinecap="square" strokeLinejoin="round"/>
      </g>
      <defs>
        <clipPath id="clip0_6_24">
          <rect width="95" height="95" rx="10" fill="white"/>
        </clipPath>
      </defs>
    </svg>

  )
}
