These icons are from Iconpark https://iconpark.oceanengine.com/home

Configured with the following:
 * Icon Size 48px
 * Stroke width 2px
 * Icon theme: Line
 * Stoke Color: #e0ff4f (Oribtal Lime)

Do not use the icons for the app, only for docs.

