import Prism from 'prismjs'
import {taxiPrismLanguage} from "@/utils/taxiPrismLanguage";

taxiPrismLanguage(Prism);

export default Prism;
