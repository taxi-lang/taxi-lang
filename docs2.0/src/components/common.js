import clsx from 'clsx'
import {Button} from './Button'
import Image from "next/future/image";
import * as React from "react";
import Head from 'next/head';
import {cn} from '@/lib/utils';
import {IconExternalLink} from '@tabler/icons-react';
import NextLink from 'next/link';


export function IconContainer({as: Component = 'div', className = '', light, dark, ...props}) {
  return (
    <Component
      className={`w-16 h-16 p-[0.1875rem] rounded-full ring-1 ring-slate-900/10 shadow overflow-hidden ${className}`}
      {...props}
    >
      {light && (
        <div
          className="aspect-w-1 aspect-h-1 bg-[length:100%] dark:hidden"
          style={{
            backgroundImage: `url(${light})`,
          }}
        />
      )}
      {dark && (
        <div
          className="hidden aspect-w-1 aspect-h-1 bg-[length:100%] dark:block"
          style={{
            backgroundImage: `url(${dark})`,
          }}
        />
      )}
    </Component>
  )
}

export function HeadMetaTags({title = "Taxi - Semantic schemas and querying"}) {
  return (
    <Head>
      <meta
        key="twitter:title"
        name="twitter:title"
        content={title}
      />
      <meta
        key="og:title"
        property="og:title"
        content={title}
      />
      <title>{title}</title>
    </Head>
  )
}

export function SectionHeading({className = '', ...props}) {
  return <h2 className={`mt-8 font-semibold font-brand text-emerald-500 text-center text-md ${className}`} {...props} />
}

export function Caption({className = '', ...props}) {
  return <h2 className={`mt-8 font-semibold font-brand ${className}`} {...props} />
}

export function SectionHeadingParagraph({className = '', ...props}) {
  return (<div className={`mt-8 mb-10 text-lg text-slate-300 lg:text-center max-w-5xl mx-auto flex flex-col gap-4 ${className}`} {...props}>
  </div>)
}

export function QuerySectionTitle({className = '', ...props}) {
  return (
    <p
      className={`mt-4 text-3xl sm:text-lg dark:text-citrus font-brand ${className}`}
      {...props}
    />
  )
}

export function QuerySectionHeader({className = '', ...props}) {
  return (
    <p
      className={`mt-4 text-3xl sm:text-xl dark:text-white font-medium font-brand ${className}`}
      {...props}
    />
  )
}

export function BigText({className = '', ...props}) {
  return (
    <p
      className={`mt-4 text-4xl lg:text-5xl text-slate-900 font-extrabold dark:text-slate-50 ${className}`}
      {...props}
    />
  )
}

export function LearnMoreLink({buttonMode = 'primary', className = '', href = '', label = 'Learn more'}) {
  const externalLinkIcon = href.includes('http') ? <IconExternalLink/> : null
  const styles = {
    'secondary' : 'mt-4 text-sky-400 hover:underline underline-offset-4 flex items-center gap-2 ',
    'primary' : 'text-citrus leading-8 my-8 h-8'
  }
  const styleClasses = styles[buttonMode] || ''
  return (
    <NextLink href={href}>
      <a href={href} className={cn('group block text-md font-semibold', styleClasses, className)} target={externalLinkIcon ? '_blank' : null}>
        <span className="flex gap-2 justify-center content-baseline">
          {label}{buttonMode === 'secondary' && externalLinkIcon}
          { buttonMode === 'primary' && (<ArrowCitrus className="-rotate-90 stroke-[6px] leading-8 ml-3 group-hover:ml-4 group-hover:stroke-[8px] transition-[stroke,margin]" width="16" height="32"></ArrowCitrus>)}
        </span>
      </a>
    </NextLink>
  )
}

export function Paragraph({as: Component = 'div', className = '', ...props}) {
  return <Component
    className={`text-lg text-center font-light leading-8 mt-8 space-y-6 font-sans text-white ${className}`} {...props} />
}

export function Link({className, ...props}) {
  return <Button className={clsx('mt-8', className)} {...props} />
}

export function InlineCode({className = '', ...props}) {
  return (
    <code
      className={`font-mono text-slate-900 font-medium dark:text-slate-200 ${className}`}
      {...props}
    />
  )
}

export {Widont} from '@/components/Widont'

export let themeTabs = {
  Simple: (selected) => (
    <>
      <path
        d="M5 11a4 4 0 0 1 4-4h30a4 4 0 0 1 4 4v26a4 4 0 0 1-4 4H9a4 4 0 0 1-4-4V11Z"
        fill="currentColor"
        fillOpacity={selected ? '.1' : '0'}
        stroke="currentColor"
        strokeWidth="2"
      />
      <path d="M15 7v34" stroke="currentColor" strokeWidth="2" strokeLinecap="round"/>
    </>
  ),
  Playful: (selected) => (
    <>
      <path d="M5 8h36v32H5V8Z" fill="currentColor" fillOpacity={selected ? '.1' : '0'}/>
      <path
        d="M42 29V11a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v26a4 4 0 0 0 4 4h19"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M33.662 41.736a1 1 0 0 1-1.1-1.322l3.08-8.68a1 1 0 0 1 1.736-.274l5.6 7.299a1 1 0 0 1-.637 1.596l-8.679 1.38Z"
        fill={selected ? 'currentColor' : 'none'}
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M14 7v34" stroke="currentColor" strokeWidth="2" strokeLinecap="round"/>
      <path
        d="M22.8 19.949a2 2 0 0 1 2.4-1.5l5.851 1.352a2 2 0 0 1 1.5 2.399l-1.352 5.851a2 2 0 0 1-2.399 1.5l-5.851-1.352a2 2 0 0 1-1.5-2.399l1.352-5.851Z"
        fill={selected ? 'currentColor' : 'none'}
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </>
  ),
  Elegant: (selected) => (
    <>
      <path
        d="M6 8h32a4 4 0 0 1 4 4v28H6V8Z"
        fill="currentColor"
        fillOpacity={selected ? '.1' : '0'}
      />
      <path
        d="M43 21v16a4 4 0 0 1-4 4H9a4 4 0 0 1-4-4V11a4 4 0 0 1 4-4h20M15 7v34"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M37 7c0 1.101 0 6-6 6 1.101 0 6 0 6 6 0-6 4.899-6 6-6-6 0-6-4.899-6-6ZM31 21c0 .734 0 4-4 4 .734 0 4 0 4 4 0-4 3.266-4 4-4-4 0-4-3.266-4-4Z"
        fill={selected ? 'currentColor' : 'none'}
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </>
  ),
  Brutalist: (selected) => (
    <>
      <path
        d="M9 41h30a4 4 0 0 0 4-4V11a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v26a4 4 0 0 0 4 4Z"
        fill="currentColor"
        fillOpacity={selected ? '.1' : '0'}
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M15 7v34M17 13h-2M43 13h-6"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M21 29V15a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-8a2 2 0 0 1-2-2Z"
        fill={selected ? 'currentColor' : 'none'}
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M25 31v2a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V19a2 2 0 0 0-2-2h-2"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
      />
    </>
  ),
}
