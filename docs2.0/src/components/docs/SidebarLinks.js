export const SidebarLinks = [
  {
    title: 'Introduction',
    links: [
      {title: 'Welcome to Taxi', href: '/docs'},
    ],
  },
  {
    title: 'Describing schemas',
    links: [
      {title: 'Basic types', href: '/docs/language/basic-types'},
      {title: 'Semantic types', href: '/docs/language/semantic-types'},
      {title: 'Models', href: '/docs/language/models'},
      {title: 'Services', href: '/docs/language/services'},
    ],
  },
  {
    title: 'Querying',
    links: [
      {title: 'Querying', href: '/docs/taxiql/querying'},
      {title: 'Mutations', href: '/docs/taxiql/mutations'},
    ],
  },
  {
    title: 'Functions and Expressions',
    links: [
      {title: 'Syntax', href: '/docs/language/functions'},
      {title: 'Taxi Stdlib', href: '/docs/language/stdlib'},
    ],
  },
  {
    title: 'Working with other API specs',
    links: [
      {title: 'Overview', href: '/docs/other-api-specs/working-with-other-api-specs'},
      {title: 'Open API', href: '/docs/other-api-specs/open-api'},
      {title: 'Avro', href: '/docs/other-api-specs/avro'},
      {title: 'Protobuf', href: '/docs/other-api-specs/protobuf'},
      {title: 'SOAP', href: '/docs/other-api-specs/soap'},
      {title: 'CSV', href: '/docs/other-api-specs/csv'},
      {title: 'XML', href: '/docs/other-api-specs/xml'},
    ],
  },
  {
    title: 'Packages',
    links: [
      {title: 'Taxi projects', href: '/docs/packages/taxi-projects'},
      {title: 'Publishing', href: '/docs/packages/publishing'},
    ]
  },
  {
    title: 'Taxi CLI',
    links: [
      {title: 'Taxi CLI', href: '/docs/taxi-cli/taxi-cli-intro'},
      {title: 'Plugins', href: '/docs/taxi-cli/plugins'},
      {title: 'Generating OpenAPI', href: '/docs/taxi-cli/openapi-plugin'},
      {title: 'Generating Kotlin', href: '/docs/taxi-cli/kotlin-plugin'},
      {title: 'Linter', href: '/docs/taxi-cli/linter'},
    ],
  },
  {
    title: 'Guides',
    links: [
      /*{title: 'Introduction to Semantic Integration', href: '/docs/background/intro-to-semantic-integration'},*/
      {title: 'Adopting semantic types', href: '/docs/language/adopting-semantic-types'},
      /*{title: 'Tips on building Taxonomies', href: '/docs/background/tips-on-taxonomies'}*/
      {title: 'Best practices for taxonomy development', href: '/docs/language/best-practices-for-taxonomy-development'},
      {title: 'Building your base taxonomy', href: '/docs/language/building-your-base-taxonomy'},
    ]
  },
]
