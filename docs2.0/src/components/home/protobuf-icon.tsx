import * as React from "react";

export default function ProtobufIcon({width,height}) {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width={width} height={height}>
      <polygon points='15.996 3.22 26.966 9.7 26.954 22.3 16 28.78 5.038 22.408 5.034 9.628 15.996 3.22'
               opacity='0.1' fill='currentColor' />
      <polygon points='25.569 13.654 19.946 16.964 19.943 24.89 25.59 21.565 25.569 13.654'
               opacity='0.3' fill='currentColor' />
      <polygon
        points='23.282 12.303 25.569 13.654 19.946 16.964 19.943 24.89 17.327 23.37 17.348 15.875 23.282 12.303'
        opacity='0.8' fill='currentColor' />
      <polygon
        points='22.512 10.35 22.514 11.816 16.411 15.498 16.418 23.597 14.998 24.431 14.994 14.856 22.512 10.35'
        opacity='0.3' fill='currentColor' />
      <polygon
        points='20.008 8.871 22.512 10.35 14.994 14.856 14.998 24.431 12.194 22.801 12.189 13.413 20.008 8.871'
        opacity='0.8' fill='currentColor' />
      <polygon points='19.226 6.606 19.226 8.374 11.21 13.074 11.21 23.172 9.808 23.988 9.835 12.277 19.226 6.606'
               opacity='0.3' fill='currentColor' />
      <polygon points='16.16 4.784 6.53 10.394 6.529 22.071 9.827 23.988 9.835 12.277 19.235 6.606 16.16 4.784'
               opacity='0.8' fill='currentColor' />
    </svg>
  )
}
