---
version: 1.51.0
aggregateVersion: 1.51
releaseDate: 2024-03-05
title: 1.51 release notes
---
## 1.51.0
2024-03-05

### Bug Fixes

* **build:** don't deploy branches in validate phase ([4b51180](https://gitlab.com/taxi-lang/taxi-lang/commit/4b5118069aa7d38f6c73d4ccebd038ee128352ec))
* **compiler:** raise error when trying to project an array to non-array (or similar) ([a1779b7](https://gitlab.com/taxi-lang/taxi-lang/commit/a1779b7eac13b46189f38fff961746ce16245cb7))
* **lang:** fix error when using a cast expression inside a given clause ([4aa45ec](https://gitlab.com/taxi-lang/taxi-lang/commit/4aa45ec62e14c1a20341189df167bcb9986e2b9e))



