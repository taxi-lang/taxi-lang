---
version: 1.36.2
aggregateVersion: 1.36
releaseDate: 2023-03-15
title: 1.36 release notes
---
## 1.36.2
2023-03-15

### Bug Fixes

* **build:** rollback base image ([23a3fd5](https://gitlab.com/taxi-lang/taxi-lang/commit/23a3fd518d202918fd46ad8d9ca5935d0fbbc4f4))
* **kotlin/codegen:** params now correctly generate semantic types ([f681f33](https://gitlab.com/taxi-lang/taxi-lang/commit/f681f337ef50322f7dfc94a92314319164a00cf2))



