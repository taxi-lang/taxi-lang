---
version: 1.48.0
aggregateVersion: 1.48
releaseDate: 2024-01-27
title: 1.48 release notes
---
## 1.48.0
2024-01-27

### Bug Fixes

* **compiler:** projections on inline types are correctly checked for type assignments ([8cf6b0d](https://gitlab.com/taxi-lang/taxi-lang/commit/8cf6b0d9c5b0c8cc1005e693ae43cb35a24d4f8f))
* **compiler:** remove inline type aliases. ([d035997](https://gitlab.com/taxi-lang/taxi-lang/commit/d03599735e74102cd94a4d26bc6aa021f8c471b9))
* **compiler:** single-arg function calls on models with inferred return type is no longer parsed as a type expression ([377949d](https://gitlab.com/taxi-lang/taxi-lang/commit/377949dc5ab2fc7b746a361ad1c844cb30e38d00))
* **stdlib/dateMath:** relaxed signatures for addDays | addMinutes etc., until we have overload support ([df29c1f](https://gitlab.com/taxi-lang/taxi-lang/commit/df29c1f312952c07917490f0cb3ce620c9097c2e))
* **stdlib:** relax the signature of concat() to ...any ([560dfbf](https://gitlab.com/taxi-lang/taxi-lang/commit/560dfbf65d49e7d754013382d270bd040410a0f7))
* **type-checker:** allow assigning strings to enums ([52ea40f](https://gitlab.com/taxi-lang/taxi-lang/commit/52ea40f8f86ddc135d2f2ddaba7444c599ec4eca))


### Features

* **compiler:** enable type-checker by default ([9e6d217](https://gitlab.com/taxi-lang/taxi-lang/commit/9e6d2178b49c9ee5497e851611467e967a341502))
* **core:** add support for casting between types ([9f4b592](https://gitlab.com/taxi-lang/taxi-lang/commit/9f4b5927162af1f2f9bf9b9e1299b4d2467a61b2))



