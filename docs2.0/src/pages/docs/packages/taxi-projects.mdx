---
title: Taxi projects & configuration
description: Layout of a taxi project, and configuring it's build
---

import {Callout} from "../../../components/docs/Callout";

## Taxi projects and files
A Taxi project contains a `taxi.conf` file, and a collection of files saved with a `.taxi.` extension.

A typical project might look like this:
```text
.
├── src/
│   └── sample.taxi
├── tests/
│   └── hello-world/
│       ├── hello-world.spec.json
│       ├── input.csv
│       └── expected-output.json
└── taxi.conf
```

Taxi files are simple text files, saved with a `.taxi` extension.

## The taxi.conf file
A `taxi.conf` file describes a project's layout, and the plugins to be invoked after compilation.  It follows the [HOCON](https://github.com/lightbend/config/blob/master/HOCON.md#hocon-human-optimized-config-object-notation) format, which is like supercharged JSON.

```hocon
name: taxi/maven-sample
version: 0.3.0
sourceRoot: src/
plugins: {
   taxi/kotlin: {
      maven: {
         groupId: "lang.taxi"
         artifactId: "parent"
         repositories: [
            {
               id: "internal-repo"
               url: "https://newcorp.nexus.com"
               snapshots: true
            }
         ]
      }
   }
}

```

The source for the config can be seen [here](https://gitlab.com/taxi-lang/taxi-lang/-/blob/develop/core-types/src/main/java/lang/taxi/packages/TaxiPackageProject.kt),
which provides the complete documentation of how projects can be configured.

| Name                | Definition                                                                                    |
|:--------------------|:----------------------------------------------------------------------------------------------|
| name                | The name of the taxi project.  By convention, follows a format of `organisation/project-name` |
| version             | The version of the taxi project                                                               |
| sourceRoot          | The root location of taxi files to be compiled                                                |
| output              | The folder where any generated artifacts will be written                                      |
| plugins             | A list of <Link to="../../plugins/README">plugins</Link> to be enabled on the project         |
| pluginSettings      | Defines where plugins are fetched and loaded from                                             |
| dependencies        | A list of other taxi project dependencies that this project depends on                        |
| repositories        | A list of taxi repositories to fetch content from                                             |
| publishToRepository | Defines where this project should be published to, should other projects wish to depend on it |
| credentials         | A list of credentials for authenticating with remote repositories                             |

### Dependencies

Projects may declare dependencies on other taxi projects.  This provides a powerful mechanism for creating modular, re-usable taxonomies across organisations.

The format for declaring dependencies is:

```hocon
dependencies: {
    // Fetch from a package manager, such as Nexus 
    "org/ProjectName" : 0.2.0

    // Fetch from a github repository, at a specific branch / tag
    "com.orbitalhq/core" : "github:orbitalapi/orbital-core-taxi#0.34.0"
   
    // Fetch from a gitlab repository (at https://gitlab.com) on the default branch
    "org.test/dependencyA" : "gitlab:taxi-lang/test-project-a"

    // Fetch from a different git provider
    "org.test/dependencyB" : "https://someGitProvider.com/taxi-lang/test-project-a.git"

}
```

For details on sharing, publishing and depending on taxonomies, see [Publishing Taxi projects](./publishing)

### Repositories
Repositories define where to try to download dependencies from (and where to publish to).  Currently, only [Sonatype Nexus](https://www.sonatype.com/nexus/repository-oss) is supported,
or pull directly from Github.

The Taxi CLI tries to download each dependency from the list of repositories until successful.

The format for declaring a repository in your `taxi.conf` file is as follows:

```
repositories: [
   {
      // The name of the repository.  Optional, but used to reference credentials
      name: "my-corporate-nexus",
      url: "http://localhost:8081",
      // Optional.  Defines the type of repository being used.  Currently only 'nexus' is supported,
      // so this can be omitted.
      type : "nexus",

      // Settings specific to the repository provider.
      // Below settings shown are applicable to a nexus repository
      settings: {
         // The name of the repository as defined within nexus
         repositoryName : taxi
      }
   }
]
```

For more information on publishing and sharing taxonomies, see [sharing projects with Nexus](/docs/packages/publishing#sharing-projects-with-nexus)

### Credentials
Credentials define how to authenticate with a repository.

The `repositoryName` attribute must match the `name` attribute of a defined repository.
```
credentials: [
   { repositoryName: "nexus", username: "jimmy", password: "pass123" }
]
```

Note - credentials don't need to be defined in your projects main `taxi.conf` - in fact, we recommend you don't.
Instead, define a configuration file at  `~/.taxi.conf` (your home directory) which contains the credentials.  See below for
more detail on merging configuration files.

### Linter config

Per-project linting rules can be specified in the `taxi.conf`, to disable or alter the severity of individual linter rules.

The format of the section is as follows:

```json
linter: {
    // The name of the linter rule.
   no-duplicate-types-on-models: {
      // Severity - optional - can be INFO | WARN | ERROR.
      // Defaults to WARN
      // Note that setting to ERROR will cause builds to fail if the rule is violated
      severity: INFO

      // enabled - optional - defaults to true
      enabled: true
   }
   no-primitive-types-on-models: {enabled: false}
}
```
Note that the entire linter section is optional.  If omitted, all rules are enabled, with the severity defined by each rule.  (By convention, this is `WARN`).

<Discourage>
   When making changes in VSCode, remember that you must save the `taxi.conf` file before changes are applied.
</Discourage>

## Merging multiple configuration files
By default, Taxi tooling will merge configuration files from the following locations:

 * The root of the project - `./taxi.conf`
 * The home directory - `~/.taxi/taxi.conf`

This allows for storing of sensitive information - such as credentials - outside of main projects, and source control.
