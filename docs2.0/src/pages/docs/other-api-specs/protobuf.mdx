---
title: Protobuf Integration
description: Learn how to embed Taxi semantic types in Protocol Buffer definitions
---

import { Callout } from '@/components/docs/Callout';

## Overview

Taxi can enhance Protocol Buffer definitions with semantic meaning, enabling:
- Semantic typing for protobuf messages and fields
- Integration with message streaming systems
- Consistent type definitions across protobuf and other formats

<Callout type="note" title="Semantic Enhancement">
  While Protocol Buffers describe message structure, Taxi adds semantic meaning to enable better data discovery and integration across systems.
</Callout>

## Adding Taxi to Protobuf

### Basic Setup

1. First, create the Taxi extension definition in your protobuf project:

```protobuf
// taxi/dataType.proto
import "google/protobuf/descriptor.proto";

package taxi;

extend google.protobuf.FieldOptions {
  optional string dataType = 50002;
}
```

2. Then use the extension to add semantic types:

```protobuf
import "taxi/dataType.proto";

message Order {
  // Add semantic type for customer name
  optional string customer_name = 1 [(taxi.dataType)="acme.CustomerName"];
  
  // Add semantic type for order ID
  optional int32 order_id = 2 [(taxi.dataType)="acme.OrderId"];
}
```

### Field Type Mapping

Taxi automatically maps protobuf field types to semantic types:

```protobuf
message Customer {
  // Maps to String-based semantic type
  optional string name = 1 [(taxi.dataType)="acme.CustomerName"];
  
  // Maps to Int-based semantic type
  optional int32 age = 2 [(taxi.dataType)="acme.CustomerAge"];
  
  // Maps to array type
  repeated string tags = 3 [(taxi.dataType)="acme.Tag"];
}
```

## Generated Taxi Models

When Taxi processes a protobuf definition, it generates corresponding Taxi models:

```taxi
@lang.taxi.formats.ProtobufMessage(
  packageName = "acme.proto",
  messageName = "Customer"
)
model Customer {
   @lang.taxi.formats.ProtobufField(tag = 1, protoType = "string") 
   name: CustomerName?
   
   @lang.taxi.formats.ProtobufField(tag = 2, protoType = "int32")
   age: CustomerAge?
   
   @lang.taxi.formats.ProtobufField(tag = 3, protoType = "string")
   tags: Tag[]
}
```

<Callout type="note" title="Reccomendation: Add taxi types to Protobuf schemas when possible">
  <p>You can generate these Protobuf models by hand - but we don't recommend it.</p>
  <p>If you have a protobuf schema and can annotate it, it's recommended to annotate that with taxi types, rather than
    duplicating the schema definition as a Taxi model.</p>
</Callout>

