---
title: OpenAPI Integration
description: Learn how to embed Taxi semantic types in OpenAPI specifications
---

import { Callout } from '@/components/docs/Callout';

## Overview

Taxi can be embedded within OpenAPI specifications to add semantic meaning to APIs. This allows you to:
- Define semantic types for request/response models
- Add semantic meaning to API parameters
- Create a unified semantic layer across multiple APIs

<Callout type="note" title="Semantic Enhancement">
  OpenAPI specs describe API structure. Taxi enhances them with semantic meaning, enabling better data discovery and integration.
</Callout>

## Adding Taxi to OpenAPI

### Basic Syntax

Taxi metadata is added using `x-taxi-type` extensions in your OpenAPI spec:

```yaml
x-taxi-type:
  name: com.acme.MyType    # The semantic type name
  create: false            # Optional flag to control type creation
```

### Type Creation Behavior

The `create` flag controls whether new types are created if they don't exist.

The default behaviour varies between models and field attributes - and defaults to the reccomended best practice
for each.

| Context          | Default | Behavior                       |
|------------------|---------|--------------------------------|
| Response Models  | `true`  | Creates new types if not found |
| Field Attributes | `false` | Requires types to exist        |

<Callout type="note" title="Type Creation">
  <p>Semantic types let you share data between systems, while avoiding tight coupling. Try to use existing types (where the semantics match), or create new types in a shared taxonomy - rather than creating types in your OpenAPI spec</p>
  <p>Only create new types (<code>create: true</code>) when you're intentionally extending your semantic model, or creating a type you're not ready to share yet.</p>
</Callout>

### Declaring write operations
Some TaxiQL behaviour depends on operations being declared as write operations (sometimes referred to as 'mutations').

To indicate that an operation is a write operation, use a `x-taxi-operation-kind` of `write` at the operation level:

```yaml
   openapi: "3.0.0"
   info:
     version: 1.0.0
     title: Swagger Petstore
   paths:
     /pets:
       post:
>        x-taxi-operation-kind: write
         responses:
           '200':
             description: successful operation
             content:
               application/json:
                 schema:
                   type: object
                   properties:
                     name:
                       type: string
```

## Enriching OpenAPI Components

### Response Models

Add semantic meaning to response types:

```yaml
   components:
     schemas:
       Customer:
>        x-taxi-type:
>          name: acme.Customer
         properties:
           id: 
             type: string
>            x-taxi-type:
>              name: acme.CustomerId
           email:
             type: string
>            x-taxi-type:
>              name: acme.EmailAddress
```

### Input Parameters

Enrich API parameters with semantic types:

```yaml
   paths:
     /customers/{id}:
       get:
         parameters:
           - name: id
             in: path
             schema:
               type: string
>              x-taxi-type:
>                name: acme.CustomerId
```

### Field Attributes

Add semantic meaning to model fields:

```yaml
   components:
     schemas:
       Customer:
         properties:
           name:
             type: string
>            x-taxi-type:
>              name: acme.CustomerName
>              # Only create if type doesn't exist
>              create: true
```

